use std::collections::HashMap;
use url::Url;

/// Manga
///
#[derive(Debug, serde::Deserialize)]
pub struct Manga {
    /// Manga ID
    ///
    pub id: u64,

    /// Link
    ///
    pub link: Url,

    /// Url Slug
    ///
    pub slug: String,

    /// Tags for this object. Only contains a unique tag for locating chapters.
    ///
    pub tags: Vec<u64>,

    /// Manga Title
    ///
    pub title: MangaTitle,

    /// Unknown KVs
    ///
    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

/// A Manga Title
///
#[derive(Debug, serde::Deserialize)]
pub struct MangaTitle {
    /// Rendered title
    ///
    pub rendered: String,

    /// Unknown KVs
    ///
    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug)]
pub struct Chapter {
    /// A Chapter title
    ///
    pub title: String,

    /// A list of chapter images
    ///
    pub images: Vec<Url>,

    /// url slug
    ///
    pub slug: String,
}
