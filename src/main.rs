use anyhow::Context;
use futures::stream::FuturesUnordered;
use futures::StreamExt;
use std::convert::TryInto;
use std::path::Path;
use std::path::PathBuf;
use std::sync::Arc;
use std::time::Instant;
use url::Url;

#[derive(argh::FromArgs)]
#[argh(description = "A tool to download manga")]
struct CommandOptions {
    #[argh(subcommand)]
    subcommand: Subcommand,
}

#[derive(argh::FromArgs, PartialEq, Debug)]
#[argh(subcommand)]
enum Subcommand {
    Search(SearchOptions),
    Download(DownloadOptions),
}

#[derive(argh::FromArgs, PartialEq, Debug)]
#[argh(subcommand, name = "search", description = "search for manga")]
pub struct SearchOptions {
    /// the search query
    #[argh(positional)]
    query: String,
}

#[derive(argh::FromArgs, PartialEq, Debug)]
#[argh(
    subcommand,
    name = "download",
    description = "download videos from mangafast"
)]
pub struct DownloadOptions {
    /// the manga id
    #[argh(positional)]
    id: u64,

    /// the dir to save the manga
    #[argh(option, short = 'o', default = "PathBuf::from(\".\")")]
    out_dir: PathBuf,
}

fn main() {
    let options: CommandOptions = argh::from_env();
    let tokio_rt = match tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
    {
        Ok(rt) => rt,
        Err(e) => {
            eprintln!("Failed to create tokio runtime: {}", e);
            return;
        }
    };

    tokio_rt.block_on(async {
        let client = mangafast::Client::new();
        match options.subcommand {
            Subcommand::Search(options) => {
                let results = match client.search(&options.query).await {
                    Ok(results) => results,
                    Err(e) => {
                        eprintln!("Failed to get results: {}", e);
                        return;
                    }
                };

                println!("Search Results");
                for (i, result) in results.list.iter().enumerate() {
                    println!("{}) {} (id: {})", i + 1, result.title.rendered, result.id);
                }
            }
            Subcommand::Download(options) => download_cli(&client, &options).await,
        }
    });
}

async fn compile_image_list(
    out_path: &Path,
    chapters: &[mangafast::Chapter],
) -> Vec<(Url, PathBuf)> {
    let mut future_set = FuturesUnordered::new();
    let mut image_list = Vec::with_capacity(chapters.len());

    for chapter in chapters.iter().rev() {
        let num_images = chapter.images.len();
        let num_images_digits = get_num_digits(num_images);

        for (i, image_url) in chapter.images.iter().enumerate() {
            let image_url = image_url.clone();
            let mut out_path = out_path.join(&chapter.slug);

            let i_digits = get_num_digits(i + 1);
            let padded_i_str = format!("{}{}", "0".repeat(num_images_digits - i_digits), i + 1);

            let extension = Path::new(image_url.path())
                .extension()
                .unwrap_or_else(|| "jpg".as_ref());

            out_path.push(padded_i_str);
            out_path.set_extension(extension);

            future_set.push(async move {
                tokio::spawn(async move {
                    if tokio::fs::metadata(&out_path).await.is_err() {
                        Some((image_url, out_path))
                    } else {
                        None
                    }
                })
                .await
            });
        }
    }

    while let Some(result) = future_set.next().await {
        match result {
            Ok(Some(entry)) => image_list.push(entry),
            Ok(None) => {}
            Err(e) => {
                eprintln!("A tokio task panicked while compiling the list: {}", e);
                eprintln!("Not all images may be downloaded!");
            }
        }
    }

    image_list
}

async fn download_cli(client: &mangafast::Client, options: &DownloadOptions) {
    eprintln!("Fetching manga...");
    let manga = match client.get_manga(options.id).await {
        Ok(info) => info,
        Err(e) => {
            eprintln!("Failed to get manga info: {}", e);
            return;
        }
    };

    let out_path = options.out_dir.join(&manga.slug);
    if let Err(e) = tokio::fs::create_dir_all(&out_path).await {
        eprintln!("Failed to create out dir: {}", e);
        return;
    }

    let manga_chapter_tag_id = match manga.tags.get(0).copied() {
        Some(id) => id,
        None => {
            eprintln!("Missing manga tag id");
            return;
        }
    };

    eprintln!("Fetching manga chapters...");
    let chapters = match client.get_all_chapters(manga_chapter_tag_id).await {
        Ok(chapters) => chapters,
        Err(e) => {
            eprintln!("Failed to get manga chapters: {}", e);
            return;
        }
    };

    let start_compile_time = Instant::now();
    eprintln!("Compiling image list...");
    let image_list = compile_image_list(&out_path, &chapters).await;
    let end_compile_time = Instant::now();
    eprintln!("Compiled in {:?}", end_compile_time - start_compile_time);

    if image_list.is_empty() {
        eprintln!("All items accounted for, exiting...");
        return;
    }

    let image_list_len: u64 = image_list
        .len()
        .try_into()
        .expect("image_list len is > u64::MAX");

    eprintln!("Need to fetch {} images...", image_list_len);

    let start_time = Instant::now();

    let mut future_set = FuturesUnordered::new();
    let download_semaphore = Arc::new(tokio::sync::Semaphore::new(10));
    for (url, path) in image_list.into_iter() {
        let client = client.clone();
        let download_semaphore = download_semaphore.clone();
        future_set.push(async move {
            tokio::spawn(download_image(client, download_semaphore, url, path)).await
        });
    }

    let pb = indicatif::ProgressBar::new(image_list_len);
    let mut errors = Vec::new();

    while let Some(result) = future_set.next().await {
        pb.inc(1);

        match result {
            Ok(Ok(())) => {}
            Ok(Err(e)) => errors.push(e),
            Err(e) => errors.push(e.into()),
        }
    }
    pb.finish();

    if !errors.is_empty() {
        eprintln!("Encountered the following errors: ");

        for e in errors.iter() {
            eprintln!("{:?}\n", e);
        }
    }

    let end_time = Instant::now();

    eprintln!("Done in {:?}", end_time - start_time);
}

async fn download_image(
    client: mangafast::Client,
    download_semaphore: Arc<tokio::sync::Semaphore>,
    url: Url,
    out_path: PathBuf,
) -> anyhow::Result<()> {
    let chapter_dir = out_path
        .parent()
        .expect("there should always be a chapter dir");
    tokio::fs::create_dir_all(&chapter_dir)
        .await
        .with_context(|| format!("Failed to create chapter dir {}", chapter_dir.display()))?;

    let permit = download_semaphore.acquire().await?;
    let mut file_buffer = Vec::with_capacity(2_000_000);
    client
        .get_to(url.as_str(), &mut file_buffer)
        .await
        .with_context(|| format!("Failed to download image {}", url.as_str()))?;

    tokio::fs::write(&out_path, &file_buffer)
        .await
        .with_context(|| format!("Failed to save image file to {}", out_path.display()))?;

    drop(permit);

    Ok(())
}

fn get_num_digits(mut n: usize) -> usize {
    if n == 0 {
        return 1;
    }

    let mut digits = 0;

    while n != 0 {
        digits += 1;
        n /= 10;
    }

    digits
}
