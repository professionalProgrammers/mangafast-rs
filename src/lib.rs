/// Api Types
///
pub mod types;

pub use crate::types::Chapter;
pub use crate::types::Manga;
pub use crate::types::MangaTitle;
use std::convert::TryInto;
use tokio::io::AsyncWrite;
use url::Url;

/// Error Type
///
#[derive(Debug, thiserror::Error)]
pub enum Error {
    /// Wordpress Error
    ///
    #[error("{0}")]
    Wordpress(#[from] wordpress::Error),

    /// Missing Field
    ///
    #[error("missing field '{0}'")]
    MissingField(&'static str),

    /// Invalid Url
    ///
    #[error("{0}")]
    InvalidUrl(#[from] url::ParseError),
}

/// A mangafast Client
///
#[derive(Debug, Clone)]
pub struct Client {
    client: wordpress::Client,
}

impl Client {
    /// Make a new [`Client`]
    ///
    pub fn new() -> Self {
        Self {
            client: wordpress::Client::new(
                "https://mangafast.net/".parse().expect("valid base url"),
            ),
        }
    }

    /// Search for manga
    ///
    pub async fn search(&self, query: &str) -> Result<wordpress::List<Manga>, Error> {
        Ok(self
            .client
            .get_types_builder("read")
            .search(query)
            .order_by(wordpress::OrderBy::Relevance)
            .send()
            .await?)
    }

    /// Fetch a [`Manga`] by id.
    ///
    pub async fn get_manga(&self, id: u64) -> Result<Manga, Error> {
        Ok(self.client.get_type_by_id("read", id).await?)
    }

    /// Get all chapters for a manga id
    ///
    pub async fn get_all_chapters(&self, id: u64) -> Result<Vec<Chapter>, Error> {
        let mut chapters = Vec::new();
        let mut offset = 0;
        let per_page = 100;

        loop {
            let result: wordpress::List<wordpress::Post> = self
                .client
                .get_posts_builder()
                .tag(id)
                .offset(offset)
                .per_page(per_page)
                .send()
                .await?;
            let total = result
                .total
                .try_into()
                .expect("More than usize::MAX total chapters");
            let num_fetched: u64 = result
                .list
                .len()
                .try_into()
                .expect("More than u64::MAX chapters fetched");

            chapters.reserve(result.list.len());
            for post in result.list.into_iter() {
                let title = post.title.rendered;
                let acf = post.unknown.get("acf").ok_or(Error::MissingField("acf"))?;

                // "WIP" pages contain no konten
                if let Some(string) = acf.get("konten").and_then(|v| v.as_str()) {
                    let images: Vec<Url> = string
                        .split('\n')
                        .filter(|s| !s.is_empty())
                        .map(Url::parse)
                        .collect::<Result<_, _>>()?;

                    let slug = post.slug;

                    chapters.push(Chapter {
                        title,
                        images,
                        slug,
                    });
                }
            }
            offset += num_fetched;

            if chapters.len() >= total {
                break Ok(chapters);
            }
        }
    }

    /// Send a GET request to a url and copy the response to the writer
    ///
    pub async fn get_to<W>(&self, url: &str, writer: W) -> Result<(), Error>
    where
        W: AsyncWrite + Unpin,
    {
        Ok(self.client.get_to(url, writer).await?)
    }
}

impl Default for Client {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[tokio::test]
    async fn it_works() {
        let client = Client::new();
        let results = client.search("tensei shitara ken").await.unwrap();
        dbg!(&results);
        let result = &results.list[0];
        dbg!(result);

        let chapters = client.get_all_chapters(result.tags[0]).await.unwrap();
        dbg!(chapters);
    }
}
