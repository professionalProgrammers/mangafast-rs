# mangafast-rs 

[![Build status](https://ci.appveyor.com/api/projects/status/226hju3mg370n749?svg=true)](https://ci.appveyor.com/project/professionalProgrammers/mangafast-rs)
[![](https://tokei.rs/b1/bitbucket/professionalProgrammers/mangafast-rs)](https://bitbucket.org/professionalProgrammers/mangafast-rs)

A Rust API for https://mangafast.net.

